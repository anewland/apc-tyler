<?php if (!defined('BASEPATH')) die();
class Admin extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('Admin_model');
	}

	public function index() {
		$this->load->view('admin/login');
	}

	public function dashboard() {
		$data['active'] = 'dashboard';
		
		$this->load->view('admin/includes/admin-header.tpl.php', $data);
			$this->load->view('admin/dashboard');
		$this->load->view('admin/includes/admin-footer.tpl.php');	
	}
	
	public function employees() {
		$data['active'] = 'employee';
		$data['employees'] = $this->Admin_model->get_employees();
		
		$this->load->view('admin/includes/admin-header.tpl.php', $data);
			$this->load->view('admin/employees');
		$this->load->view('admin/includes/admin-footer.tpl.php');			
	}
	
		public function employee_edit($eid) {
			$data['active'] = 'employee';
			$data['employee'] = $this->Admin_model->get_employee($eid);
			
			$this->load->view('admin/includes/admin-header.tpl.php', $data);
				$this->load->view('admin/employee_edit');
			$this->load->view('admin/includes/admin-footer.tpl.php');			
		}

		public function employee_add() {
			$data['active'] = 'employee';
			$data['employee'] = '';
			
			$this->load->view('admin/includes/admin-header.tpl.php', $data);
				$this->load->view('admin/employee_edit');
			$this->load->view('admin/includes/admin-footer.tpl.php');			
		}
		
		public function employee_update($eid) {
			$is_visible = ($this->input->post('visible') == 'on') ? '1' : '0';
						
			$data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'sort' => $this->input->post('sort'),
				'title' => $this->input->post('title'),
				'email' => $this->input->post('email'),
				'photo' => $this->input->post('photo'),
				'employment_year' => $this->input->post('employment_year'),
				'bio' => $this->input->post('bio'),
				'visible' => $is_visible
			);
	
			$this->db->where('eid', $eid);
			$this->db->update('employees', $data);
			redirect('/admin/employees');			
		
			//print_r($data);
			//print_r($_POST);
		}

		public function employee_addition() {
			$is_visible = ($this->input->post('visible') == 'on') ? '1' : '0';
			
			$data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'title' => $this->input->post('title'),
				'email' => $this->input->post('email'),
				'sort' => $this->input->post('sort'),
				'employment_year' => $this->input->post('employment_year'),
				'bio' => $this->input->post('bio'),
				'visible' => $is_visible
			);
	
			$this->db->insert('employees', $data);
			redirect('/admin/employees');			

			//print_r($data);
			//print_r($_POST);
		}

	public function faqs() {
		$data['active'] = 'faqs';
		
		$this->load->view('admin/includes/admin-header.tpl.php', $data);
			$this->load->view('admin/faqs');
		$this->load->view('admin/includes/admin-footer.tpl.php');			
	}

	public function quotes() {
		$data['active'] = 'quotes';
		
		$this->load->view('admin/includes/admin-header.tpl.php', $data);
			$this->load->view('admin/quotes');
		$this->load->view('admin/includes/admin-footer.tpl.php');			
	}

	public function testimonials() {
		$data['active'] = 'testimonials';
		
		$this->load->view('admin/includes/admin-header.tpl.php', $data);
			$this->load->view('admin/testimonials');
		$this->load->view('admin/includes/admin-footer.tpl.php');			
	}

	public function applicants() {
		$data['active'] = 'applicants';
		$data['applicants'] = $this->Admin_model->get_applicants();
		
		$this->load->view('admin/includes/admin-header.tpl.php', $data);
			$this->load->view('admin/applicants');
		$this->load->view('admin/includes/admin-footer.tpl.php');			
	}
	
	
	
	
	
	
	
	
	
	public function editDB() {
		$sql = "UPDATE employees
			SET visible='1'
			WHERE first_name ='Milton';";
		
		$this->db->query($sql);
		
	}
	
	function downloadDB() {
		$username = "apctyler"; 
		$password = "dt6CY3Rs6u9EX1ogeBgL"; 
		$hostname = "db01.internal.groupm7.com"; 
		$dbname   = "apctyler";
		 
		header('Content-type: application/force-download');
		header('Content-Disposition: attachment; filename="dbbackup.sql.gz"');
		passthru("mysqldump --user=apctyler --host=db01.internal.groupm7.com --password=dt6CY3Rs6u9EX1ogeBgL apctyler | gzip");	
	}
	
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */