<?php if (!defined('BASEPATH')) die();
class Services extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['pageTitle'] = 'APC Plumbing Services';
		$data['slider'] = FALSE;
		
		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services');
		$this->load->view('includes/footer.tpl.php');
	}

	public function plumbingrepairs() {
		$data['pageTitle'] = 'Services | Plumbing Repairs & Replacement';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/plumbing');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function waterheaters() {
		$data['pageTitle'] = 'Services | Water Heater Repair & Replacement';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/waterheaters');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function disposals() {
		$data['pageTitle'] = 'Services | Garbage Disposal Repair & Replacement';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/disposals');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function draincleaning() {
		$data['pageTitle'] = 'Services | Sewer & Drain Cleaning';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/draincleaning');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function gaslines() {
		$data['pageTitle'] = 'Services | Gas Line Services';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/gaslines');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function commercial() {
		$data['pageTitle'] = 'Services | Commercial Services';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/commercial');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	// NEWLY ADDED
	public function water_leak_repair() {
		$data['pageTitle'] = 'Services | Water Leak Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/waterleakrepair');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}
	
	public function gas_leak_repair() {
		$data['pageTitle'] = 'Services | Gas Leak Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/gasleakrepair');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function slab_leak_repair() {
		$data['pageTitle'] = 'Services | Slab Leak Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/slableakrepair');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function bathtub_faucet_repair() {
		$data['pageTitle'] = 'Services | Bathtub Faucet Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/faucetrepair-bathtub');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function shower_faucet_repair() {
		$data['pageTitle'] = 'Services | Shower Faucet Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/faucetrepair-shower');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function sink_faucet_repair() {
		$data['pageTitle'] = 'Services | Sink Faucet Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/faucetrepair-sink');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function lavatory_faucet_repair() {
		$data['pageTitle'] = 'Services | Lavatory Faucet Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/faucetrepair-lavatory');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function toilet_repair() {
		$data['pageTitle'] = 'Services | Toilet Repair';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/toilet-repair');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function bathroom_remodeling() {
		$data['pageTitle'] = 'Services | Bathroom Remodeling Service';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/bathroom-remodeling');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}

	public function pressure_test_plumbing() {
		$data['pageTitle'] = 'Services | Pressure Test Plumbing';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/services/pressure-test-plumbing');
			$this->load->view('pages/services/all');
		$this->load->view('includes/footer.tpl.php');
	}			
}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */