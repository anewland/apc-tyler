<?php if (!defined('BASEPATH')) die();
class Contact extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper('url', 'form', 'email', 'date');
	}

	public function index() {
		date_default_timezone_set('America/Chicago');
		$this->load->library('form_validation', 'email');
		
		// Form validation
		$this->form_validation->set_rules('fullname', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[10]|max_length[14]');
		$this->form_validation->set_rules('message', 'Message', 'required');
		$this->form_validation->set_rules('g-recaptcha-response', 'CAPTCHA', 'required');

		// Process if validated
		if ($this->form_validation->run() == FALSE) {
			$data['formsubmit'] = FALSE;
		} else {
			$data = array(
				'name' => $this->input->post('fullname'),
				'company' => $this->input->post('company'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'message' => $this->input->post('message'),
				'date' => mdate('%Y-%m-%d %H:%i:%s', now())
			);

			//$this->db->insert('quotes', $data);

			// Send email to APC & Client
			$data['emaildata'] = $_POST;
			$timeofday = mdate('%h', now());
			if ( $timeofday >= 5 && $timeofday <= 11 ) {
				$data['greeting'] = "Good Morning";
			} else if ( $timeofday >= 12 && $timeofday <= 18 ) {
				$data['greeting'] = "Good Afternoon";
			} else if ( $timeofday >= 19 || $timeofday <= 4 ) {
				$data['greeting'] = "Good Evening";
			}

			$html_body = $this->load->view('emails/contact', $data, TRUE);

			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('cs@apctyler.com', 'American Plumbing Company');
			$this->email->to($this->input->post('email'));
			$this->email->bcc('cs@apctyler.com');

			$this->email->subject('APC Contact Form');
			$this->email->message($html_body);
			$this->email->set_alt_message('Thank you for contacting American Plumbing Company.');

			// Send the Email
			$this->email->send();

			$data['formsubmit'] = TRUE;
		}

		$data['pageTitle'] = 'Contacting American Plumbing Co';
		$data['slider'] = FALSE;
		
		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/contact');
		$this->load->view('includes/footer.tpl.php');
	}

	public function quote() {
		date_default_timezone_set('America/Chicago');
		$this->load->library('form_validation', 'email');

		// Form validation
		$this->form_validation->set_rules('fullname', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[10]|max_length[14]');
		$this->form_validation->set_rules('address', 'Site Address', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('g-recaptcha-response', 'CAPTCHA', 'required');

		// Process if validated
		if ($this->form_validation->run() == FALSE) {
			$data['formsubmit'] = FALSE;
		} else {
			$data = array(
				'name' => $this->input->post('fullname'),
				'company' => $this->input->post('company'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'address' => $this->input->post('address'),
				'type' => $this->input->post('type'),
				'description' => $this->input->post('description'),
				'date' => mdate('%Y-%m-%d %H:%i:%s', now())
			);

			$this->db->insert('quotes', $data);

			// Send email to APC & Client
			$data['emaildata'] = $_POST;
			$timeofday = mdate('%h', now());
			if ( $timeofday >= 5 && $timeofday <= 11 ) {
				$data['greeting'] = "Good Morning";
			} else if ( $timeofday >= 12 && $timeofday <= 18 ) {
				$data['greeting'] = "Good Afternoon";
			} else if ( $timeofday >= 19 || $timeofday <= 4 ) {
				$data['greeting'] = "Good Evening";
			}

			$html_body = $this->load->view('emails/quote', $data, TRUE);

			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('cs@apctyler.com', 'American Plumbing Company');
			$this->email->to($this->input->post('email'));
			$this->email->bcc('cs@apctyler.com');

			$this->email->subject('APC Quote Request: '.$this->input->post('type'));
			$this->email->message($html_body);
			$this->email->set_alt_message('Thank you for contacting American Plumbing Company.');

			// Send the Email
			$this->email->send();

			$data['formsubmit'] = TRUE;
		}

		$data['pageTitle'] = 'Get Your Quote Today';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/quote');
		$this->load->view('includes/footer.tpl.php');
	}

	public function send() {
		$this->load->view('emails/quote');
	}
	
	public function employment() {
		date_default_timezone_set('America/Chicago');
		$this->load->library('form_validation', 'email');
		
		// Form validation
		$this->form_validation->set_rules('fullname', 'Full Name', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		$this->form_validation->set_rules('zip', 'Zip Code', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[10]|max_length[14]');
		$this->form_validation->set_rules('license_num', 'Plumbers License #', 'required');
		$this->form_validation->set_rules('education', 'Education', 'required');
		$this->form_validation->set_rules('position_of_interest', 'Position(s) of Interest', 'required');
		$this->form_validation->set_rules('date_avaliable', 'Date Avaliable', 'required');
		$this->form_validation->set_rules('skills', 'Skills', 'required');
		$this->form_validation->set_rules('g-recaptcha-response', 'CAPTCHA', 'required');

		// Process if validated
		if ($this->form_validation->run() == FALSE) {
			$data['formsubmit'] = FALSE;
		} else {
			$data = array(
				'fullname' => $this->input->post('fullname'),
				'date_applied' => mdate('%Y-%m-%d %H:%i:%s', now()),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'zip' => $this->input->post('zip'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'license_num' => $this->input->post('license_num'),
				'education' => $this->input->post('education'),
				'position_of_interest' => $this->input->post('position_of_interest'),
				'date_avaliable' => $this->input->post('date_avaliable'),
				'skills' => $this->input->post('skills')
			);

			$this->db->insert('applicants', $data);

			// Send email to APC & Client
			$data['emaildata'] = $_POST;
			$timeofday = mdate('%h', now());
			if ( $timeofday >= 5 && $timeofday <= 11 ) {
				$data['greeting'] = "Good Morning";
			} else if ( $timeofday >= 12 && $timeofday <= 18 ) {
				$data['greeting'] = "Good Afternoon";
			} else if ( $timeofday >= 19 || $timeofday <= 4 ) {
				$data['greeting'] = "Good Evening";
			}

			$html_body = $this->load->view('emails/employment', $data, TRUE);

			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('cs@apctyler.com', 'American Plumbing Company');
			$this->email->to($this->input->post('email'));
			$this->email->bcc('cs@apctyler.com');

			$this->email->subject('APC Online Employment Application');
			$this->email->message($html_body);
			$this->email->set_alt_message('Thank you for applying to work for American Plumbing Company.');

			// Send the Email
			$this->email->send();

			$data['formsubmit'] = TRUE;
		}

		$data['pageTitle'] = 'Online Employment Application';
		$data['slider'] = FALSE;
		
		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/employment');
		$this->load->view('includes/footer.tpl.php');
	}
}

/* End of file Contact.php */
/* Location: ./application/controllers/Contact.php */