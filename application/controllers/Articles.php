<?php if (!defined('BASEPATH')) die();
class Articles extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['pageTitle'] = 'American Plumbing Co Articles';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/blog/index');
		$this->load->view('includes/footer.tpl.php');
    }

    public function plumbing_resolutions() {
		$data['pageTitle'] = '11 Easy Plumbing System Resolutions For 2019 That You Can Start Today | American Plumbing Co';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/blog/resolutions');
		$this->load->view('includes/footer.tpl.php');
	}

}

/* End of file Articles.php */
/* Location: ./application/controllers/Articles.php */
