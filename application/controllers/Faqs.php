<?php if (!defined('BASEPATH')) die();
class Faqs extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['pageTitle'] = 'Frequently Asked Plumbing Questions';
		$data['slider'] = FALSE;

		$data['faqs'] = $this->apc_model->get_faqs();
		
		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/faqs');
		$this->load->view('includes/footer.tpl.php');
	}
}

/* End of file Faqs.php */
/* Location: ./application/controllers/Faqs.php */