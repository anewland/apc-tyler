<?php if (!defined('BASEPATH')) die();
class Welcome extends CI_Controller {

	public function index() {
		$data['pageTitle'] = 'Servicing East Texas\' Plumbing Needs';
		$data['slider'] = TRUE;

        $data['testimonials'] = $this->apc_model->get_testimonials();
		
		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('index');
		$this->load->view('includes/footer.tpl.php');
	}
}

/* End of file Welcome.php */
/* Location: ./application/controllers/Welcome.php */