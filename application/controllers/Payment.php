<?php if (!defined('BASEPATH')) die();
class Payment extends CI_Controller {

	public function index() {
		date_default_timezone_set('America/Chicago');
		$this->load->library('form_validation', 'email');
		$this->load->library('Authorize_net');
		
		// Form validation
		$this->form_validation->set_rules('x_card_num', 'Credit Card Number', 'required|min_length[16]');
		$this->form_validation->set_rules('x_exp_date', 'Exp. Date', 'required');
		$this->form_validation->set_rules('x_card_code', 'CVC', 'required|min_length[3]');
		$this->form_validation->set_rules('x_invoice_num', 'Invoice Number', 'required');
		$this->form_validation->set_rules('x_amount', 'Invoice Amount', 'required');

		
		$this->form_validation->set_rules('x_first_name', 'First Name', 'required');
		$this->form_validation->set_rules('x_last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('x_address', 'Address', 'required');
		$this->form_validation->set_rules('x_city', 'City', 'required');
		$this->form_validation->set_rules('x_state', 'State', 'required');
		$this->form_validation->set_rules('x_zip', 'Zip Code', 'required');
		$this->form_validation->set_rules('x_email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('x_phone', 'Phone Number', 'required|min_length[10]|max_length[14]');
		
		//$this->form_validation->set_rules('g-recaptcha-response', 'CAPTCHA', 'required');
		
			
		// Process if validated
		if ($this->form_validation->run() == FALSE) {
			$data['formsubmit'] = FALSE;
		} else {
			$auth_net = array(
				'x_card_num'			=> $this->input->post('x_card_num'),
				'x_exp_date'			=> $this->input->post('x_exp_date'),
				'x_card_code'			=> $this->input->post('x_card_code'),
				'x_invoice_num'			=> $this->input->post('x_invoice_num'),
				'x_amount'				=> $this->input->post('x_amount'),
				'x_first_name'			=> $this->input->post('x_first_name'),
				'x_company'				=> $this->input->post('x_company'),
				'x_last_name'			=> $this->input->post('x_last_name'),
				'x_address'				=> $this->input->post('x_address'),
				'x_city'				=> $this->input->post('x_city'),
				'x_state'				=> $this->input->post('x_state'),
				'x_zip'					=> $this->input->post('x_zip'),
				'x_country'				=> 'US',
				'x_phone'				=> $this->input->post('x_phone'),
				'x_email'				=> $this->input->post('x_email'),
				'x_customer_ip'			=> $this->input->ip_address(),
				);
			$this->authorize_net->setData($auth_net);
			
			// Try to AUTH_CAPTURE
			if( $this->authorize_net->authorizeAndCapture() )
			{
				$data['payment_message'] = '<h2>Your Payment is being Processed!</h2>';
				$data['payment_message'] .= '<p>Approval Code: ' . $this->authorize_net->getApprovalCode() . '</p>';
			}
			else
			{
				$data['payment_message'] = '<h2>Fail!</h2>';
				// Get error
				$data['payment_message'] .= '<p>' . $this->authorize_net->getError() . '</p>';
				// Show debug data
				//$this->authorize_net->debug();
			}			

			// Send email to APC & Client
			/*$data['emaildata'] = $_POST;
			$timeofday = mdate('%h', now());
			if ( $timeofday >= 5 && $timeofday <= 11 ) {
				$data['greeting'] = "Good Morning";
			} else if ( $timeofday >= 12 && $timeofday <= 18 ) {
				$data['greeting'] = "Good Afternoon";
			} else if ( $timeofday >= 19 || $timeofday <= 4 ) {
				$data['greeting'] = "Good Evening";
			}

			$html_body = $this->load->view('emails/payment', $data, TRUE);

			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('cs@apctyler.com', 'American Plumbing Company');
			$this->email->to($this->input->post('email'));
			$this->email->bcc('cs@apctyler.com');

			$this->email->subject('APC Contact Form');
			$this->email->message($html_body);
			$this->email->set_alt_message('Thank you for contacting American Plumbing Company.');

			// Send the Email
			$this->email->send();*/

			$data['formsubmit'] = TRUE;
		}

		$data['pageTitle'] = 'Online Payments';
		$data['slider'] = FALSE;
		
		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/payment');
		$this->load->view('includes/footer.tpl.php');
	}
}

/* End of file Payment.php */
/* Location: ./application/controllers/Payment.php */