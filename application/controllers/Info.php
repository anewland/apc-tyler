<?php if (!defined('BASEPATH')) die();
class Info extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['pageTitle'] = 'About American Plumbing Co';
		$data['slider'] = FALSE;

		$data['employees'] = $this->apc_model->get_employees();

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/about');
		$this->load->view('includes/footer.tpl.php');
	}

	public function member($eid) {
		$data['pageTitle'] = 'Meet The Team';
		$data['slider'] = FALSE;

		$data['employee'] = $this->apc_model->get_employee($eid);
		$data['employees'] = $this->apc_model->get_employees();
		$echeck = $this->apc_model->get_employee($eid);

		if($echeck->visible == 1) {
			$this->load->view('includes/header.tpl.php', $data);
				$this->load->view('pages/employee');
				$this->load->view('includes/footer.tpl.php');
		} else {
			//echo 'redirect';
			redirect('/about/', 'location', 301);
		}
	}

	public function privacy_policy() {
		$data['pageTitle'] = 'Privacy Policy';
		$data['slider'] = FALSE;

		$this->load->view('includes/header.tpl.php', $data);
			$this->load->view('pages/privacy-policy');
		$this->load->view('includes/footer.tpl.php');
	}
}

/* End of file Info.php */
/* Location: ./application/controllers/Info.php */
