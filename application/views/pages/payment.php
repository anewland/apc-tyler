<div class="row drain" id="about">
    <h1 class="text-center">Online Payments <span>Paying your plumber has never been so easy</span></h1>
</div>

<div class="row sidebar" id="form">
    <div class="col-sm-12 col-md-4">
        <div class="text-center">
        <script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?code=1182074b98e54492bf7c7f9fb225fb8e"></script></div>

        <img src="/assets/images/creditcards.png" style="width:100%; margin:10px 0;">

        <div class="break"></div>

        <h4>Contact Information</h4>
        <p><a href="tel:9035925508">(903) 592-5508</a> – Office<br/>
			(903) 592-6055 – Fax<br/>
			<a href="tel:9037801512">(903) 780-1512</a> – 24-Hour Emergency Service</p>

		<p> Customer Service: <a href="mailto:cs@apctyler.com">cs@apctyler.com</a><br/>
			Human Resources: <a href="mailto:hr@apctyler.com">hr@apctyler.com</a></p>

		<div class="break"></div>

        <h4>Business Hours</h4>
        <p><b>Monday - Thursday:</b> 7:30am - 4:30pm<br/>
        	<b>Friday:</b> 7:30am - 3:00pm</p>

        <div class="break"></div>

        <p><i>All Sales are final.</i></p>
        <h4>Refund Policy</h4>
        <p>Refunds are handled on a case by case basis.</p>

        <h4>Privacy Policy</h4>
				<p>Read our <strong><a href="/info/privacypolicy/" title="Employment">Privacy Policy</a></strong> <br/>For your privacy we do not sell or share any personal info.</p>
    </div>

    <div class="col-sm-12 col-md-8 form">
        <?php if($formsubmit == FALSE): ?>

            <?php if(validation_errors()): ?>
                <div class="alert alert-danger" role="alert"><?=validation_errors(); ?></div>
            <?php endif; ?>

            <?=form_open('/payment'); ?>
            	<div class="row section_head"><h4>Payment Information</h4></div>
                <div class="row">
                    <div class="col-md-7">
		                <div class="form-group">
		                    <label for="x_card_num">Credit Card Number <small>*</small></label>
		                    <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                                <input type="text" name="x_card_num" class="form-control" id="x_card_num" placeholder="**** **** **** ****">
                            </div>
		                </div>
                    </div>

                    <div class="col-md-3">
		                <div class="form-group">
		                    <label for="x_exp_date">Exp. Date <small>*</small></label>
		                    <input type="text" name="x_exp_date" class="form-control" id="x_exp_date" placeholder="MM/YY">
		                </div>
                    </div>

                    <div class="col-md-2">
		                <div class="form-group">
		                    <label for="x_card_code">CVC <small>*</small></label>
		                    <input type="text" name="x_card_code" class="form-control" id="x_card_code" placeholder="CVC">
		                </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="x_invoice_num">Invoice Number <small>*</small></label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-archive"></i></div>
                                <input type="text" name="x_invoice_num" class="form-control" id="x_invoice_num" placeholder="Your Invoice Number">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="x_amount">Invoice Amount <small>*</small></label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                <input id="x_amount" name="x_amount" type="text" class="form-control" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                </div>

				<div class="break"></div>

            	<div class="row section_head"><h4>Billing Information</h4></div>
                <div class="row">
                    <div class="col-md-6">
		                <div class="form-group">
		                    <label for="x_first_name">First Name <small>*</small></label>
		                    <input type="text" name="x_first_name" class="form-control" id="x_first_name" placeholder="Your First Name">
		                </div>
                    </div>
                    <div class="col-md-6">
		                <div class="form-group">
		                    <label for="x_last_name">Last Name <small>*</small></label>
		                    <input type="text" name="x_last_name" class="form-control" id="x_last_name" placeholder="Your Last Name">
		                </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="x_company">Company Name</label>
                    <input type="text" name="x_company" class="form-control" id="x_company" placeholder="Your Company Name">
                </div>

                <div class="form-group">
                    <label for="x_address">Street Address <small>*</small></label>
                    <input type="text" name="x_address" class="form-control" id="x_address" placeholder="Your Street Address">
                </div>

                <div class="row">
                    <div class="col-md-6">
		                <div class="form-group">
		                    <label for="x_city">City <small>*</small></label>
		                    <input type="text" name="x_city" class="form-control" id="x_city" placeholder="City" value="<?=set_value('city'); ?>">
		                </div>
                    </div>

                    <div class="col-md-2">
		                <div class="form-group">
		                    <label for="x_state">State <small>*</small></label>
		                    <select class="form-control" name="x_state">
								<option value="AL">AL</option>
								<option value="AK">AK</option>
								<option value="AZ">AZ</option>
								<option value="AR">AR</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DE">DE</option>
								<option value="DC">DC</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="IA">IA</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="ME">ME</option>
								<option value="MD">MD</option>
								<option value="MA">MA</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MS">MS</option>
								<option value="MO">MO</option>
								<option value="MT">MT</option>
								<option value="NE">NE</option>
								<option value="NV">NV</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NY">NY</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX" selected>TX</option>
								<option value="UT">UT</option>
								<option value="VT">VT</option>
								<option value="VA">VA</option>
								<option value="WA">WA</option>
								<option value="WV">WV</option>
								<option value="WI">WI</option>
								<option value="WY">WY</option>
							</select>
		                </div>
                    </div>

                    <div class="col-md-4">
		                <div class="form-group">
		                    <label for="x_zip">Zip Code <small>*</small></label>
		                    <input type="text" name="x_zip" class="form-control" id="x_zip" placeholder="Zip Code" value="<?=set_value('zip'); ?>">
		                </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="x_email">Email Address <small>*</small></label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input type="email" name="x_email" class="form-control" id="x_email" placeholder="Your Email Address" value="<?=set_value('email'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="x_phone">Phone Number <small>*</small></label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                <input id="x_phone" name="x_phone" type="text" class="form-control" placeholder="(903) 555-5555" value="<?=set_value('phone'); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6Lca-wETAAAAAPylyVRwKMHs_nALwER2qwmTbVRj"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>
                </div>

            <?=form_close(); ?>

        <?php else: ?>
            <div class="alert alert-info" role="alert">
	            <?=$payment_message?>
            </div>

        <?php endif; ?>
    </div>
</div>
