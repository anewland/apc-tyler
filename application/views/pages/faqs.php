<div class="row drain" id="about">
    <h1 class="text-center">Frequently Asked Plumbing Questions <span>You ask, We answer</span></h1>
</div>

<div class="row interior" id="faqs">
    <?php foreach($faqs as $f): ?>
        <div class="col-md-12">
            <h2><i class="fa fa-comments"></i> <?=$f['question'];?></h2>
            <?php if($f['photo']):?>
                <img src="/assets/images/<?=$f['photo'];?>" class="img-responsive pull-right hidden-xs img-rounded" />
            <?php endif;?>
            <p class="lead"><?=nl2br($f['answer']);?></p>
        </div>
    <?php endforeach; ?>
</div>