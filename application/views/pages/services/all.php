
<div class="row interior grey" id="services">
    <div class="col-xs-12 col-sm-2 service-area">
        <a href="/services">
            <img src="/assets/images/service/plumbingrepairs.jpg" class="img-responsive" />
            <div><span>Plumbing Repairs</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-2 service-area">
        <a href="/services">
            <img src="/assets/images/service/waterheater.jpg" class="img-responsive" />
            <div><span>Water Heater Repairs</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-2 service-area">
        <a href="/services">
            <img src="/assets/images/service/garbagedisposal.jpg" class="img-responsive" />
            <div><span>Garbage Disposal Repairs</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-2 service-area">
        <a href="/services">
            <img src="/assets/images/service/sewer.jpg" class="img-responsive" />
            <div><span>Sewer &amp; Drain Cleaning</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-2 service-area">
        <a href="/services">
            <img src="/assets/images/service/gasline.jpg" class="img-responsive" />
            <div><span>Gas Line Services</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-2 service-area">
        <a href="/services">
            <img src="/assets/images/service/commercial.jpg" class="img-responsive" />
            <div><span>Commercial Services</span></div>
        </a>
    </div>

</div> <!-- /.services -->