<div class="row drain" id="about">
    <h1 class="text-center">Complete Professional Services <span>What we can do to help you</span></h1>
</div>

<div class="row interior more-padding" id="service-desc" style="border-bottom:none;">

	<div class="col-md-9">	
		<h2>Residential &amp; Commercial Plumbing Services We Perform:</h2>
		
		<ul>
			<li>Water leaks - locate &amp; repair</li>
			<li>Gas leaks - locate &amp; repair</li>
			<li>Slab leaks - locate &amp; repair</li>
			<li>Water heater - repair or replace</li>
				<ul>
					<li>Conventional</li>
					<li>Tankless</li>
				</ul>
			<li>Faucets - repair or replace</li>
				<ul>
					<li>Tub</li>
					<li>Shower</li>
					<li>Sink</li>
					<li>Lavatory</li>
					<li>Hydrants</li>
				</ul>
			<li>Commodes - repair or replace</li>
			<li>Garbage Disposals - replace</li>
			<li>Sewer &amp; Drains - unstop or repair</li>
			<li>Sewer odors - locate and replace</li>
			<li>Piping - repair or replace</li>
			<li>Bathroom Remodeling - plan &amp; execute</li>
			<li>Pressure Testing</li>
			<li>Backflow - equipment testing</li>
		</ul>
		
		
		<h3 class="lead">How we price out our work:</h3>
		<p>We believe that the time-tested method of time and materials is the fairest way to 
			compensate for our services.  This method avoids the other practice of some companies
			that use national averages and higher profit multipliers that allow for difficulties that 
			often don't materialize.  Time and material pricing reflects only the actual time it takes
			and the materials used for the job; and because our licensed plumbers have good work ethics, you
			the customer, get a reasonably priced job.
		</p>
		<p><b><i>American Plumbing Company offers Senior and Military Discounts.</i></b></p>
		
		
		<h3 class="lead">Our Warranty</h3>
			<p>Because our licensed plumbers take pride in their work, we can offer you one of the very best 
				warranties in the business &mdash; a full one year guarantee that our repairs will 
				be free of defects in workmanship &amp; materials.  For our drain cleaning services,
				we will evaluate the condition of your pipe &amp; offer the warranty that seems
				most fair to both you and our company.
			</p>
		
	</div>
	
	<div class="col-md-3 clearfix">
	</div>
	
	
	<div class="col-md-12">
		<h6>Whether commercial or residential, American Plumbing gets the job done right. A full one-year warranty on all repairs is just one more reason to place your confidence in American Plumbing Company today!</h6>
	</div>

</div>

<div class="row interior" id="services">	
	
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services">
            <img src="/assets/images/service/plumbingrepairs.jpg" class="img-responsive" />
            <div><span>Plumbing Repairs <br/>&amp; Replacement</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services">
            <img src="/assets/images/service/waterheater.jpg" class="img-responsive" />
            <div><span>Water Heater <br/>Repair &amp; Replacement</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services">
            <img src="/assets/images/service/garbagedisposal.jpg" class="img-responsive" />
            <div><span>Garbage Disposal <br/>Repair &amp; Replacement</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services">
            <img src="/assets/images/service/sewer.jpg" class="img-responsive" />
            <div><span>Sewer &amp; Drain <br/>Cleaning</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services">
            <img src="/assets/images/service/gasline.jpg" class="img-responsive" />
            <div class="uno"><span>Gas Line Services</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services">
            <img src="/assets/images/service/commercial.jpg" class="img-responsive" />
            <div class="uno"><span>Commercial Services</span></div>
        </a>
    </div>

</div> <!-- /.services -->