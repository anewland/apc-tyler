<div class="row drain" id="about">
    <h1 class="text-center">About American Plumbing Co <span>Our plumbers, your friends</span></h1>
</div>

<div class="row interior" id="employee-info" style="border:none;">
    <img class="ephoto img-responsive" src="/assets/images/teamphoto-2018.png?1522940306" alt="The American Plumbing Family" />
    <div class="pull-left" style="margin:0 20px 0;">
	    <!-- Begin eLocalLink Code :twn714vp3c-31744 -->
		<a href="javascript:;" onClick="window.open('http://www.elocallink.tv/vp6/spon-fcsa_a.php?sponid=AzZUZQZlWTVUYg==&fvm=1','elocallinktv','scrollbars=yes,resizable=yes,width=716,height=540,left=0,top=0,ScreenX=0,ScreenY=0');";><img src="http://elocallink.tv/vp2/images2/linklogo.jpg" width="122" height="100" border="0"></a>
		<!-- End eLocalLink Code :twn918vp2 -->
    </div>
    <p>American Plumbing Company has been serving East Texas since 1986, having been founded by Master Plumber, Milton Vanderpool of Tyler as Vanderpool Plumbing Company. Presently, American Plumbing has a complete staff of plumbing technicians available to provide quick response to your needs. American Plumbing specializes in plumbing repairs and remodeling services, and is competent in both commercial and residential arenas. Out of a commitment to save you time and money, a significant inventory of parts and equipment is kept on company trucks with more in the company warehouse. American Plumbing licensed plumbers are well trained and are available after regular hours as needed.</p>
    <p>Most importantly, American Plumbing's goal is to serve your needs with pride and integrity in such a way that you sense the company's commitment to please you and, ultimately, God.</p>
    <p>American Plumbing Company has a full one-year warranty on all its repairs! American Plumbing would consider it a privilege to be considered your plumber and looks forward to gaining your confidence.</p>

</div>

<div class="row interior" id="team">
    <?php $n=0;?>
    <?php foreach($employees as $e): ?>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 employee">
            <a href="/about/<?=strtolower(str_replace('.', '', $e['first_name']))?>-<?=strtolower($e['last_name'])?>/">
                <?php if($e['photo'] != NULL): ?>
                	<img src="/assets/images/employees/<?=$e['photo']?>" class="img-rounded" />
                <?php else: ?>
                	<img src="/assets/images/employees/default.jpg" class="img-rounded" />
                <?php endif; ?>
                <h3><?=$e['first_name']?></h3>
            </a>
        </div>
        <?php $n++ ?>
    <?php endforeach; ?>
</div>


<div class="row" id="map">

</div>




