<div class="row drain" id="about">
    <h1 class="text-center">About American Plumbing Co <span>Our plumbers, your friends</span></h1>
</div>


<div class="row interior" id="employee-info">
	<?php if($employee->photo != NULL): ?>
	    <img class="ephoto img-responsive" src="/assets/images/employees/<?=$employee->photo?>" alt="<?=$employee->first_name. ' ' .$employee->last_name?>" />
    <?php endif; ?>
    <h2><?=$employee->first_name.' '.$employee->last_name?> <span class="lead"><?=$employee->title?></span></h2>
    <p><?=$employee->bio?></p>
    <?php if($employee->employment_year): ?>
    	<p><?=$employee->first_name?> has been with American Plumbing Co. since <?=$employee->employment_year?>.
	<?php endif; ?>
</div>


<div class="row interior grey" id="team">
    <?php $n=0;?>
    <?php foreach($employees as $e): ?>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 employee">
            <a href="/about/<?=strtolower(str_replace('.', '', $e['first_name']))?>-<?=strtolower($e['last_name'])?>/">
                <?php if($e['photo'] != NULL): ?>
                	<img src="/assets/images/employees/<?=$e['photo']?>" class="img-rounded" />
                <?php else: ?>
                	<img src="/assets/images/employees/default.jpg" class="img-rounded" />
                <?php endif; ?>
                <h3><?=$e['first_name']?></h3>
            </a>
        </div>
        <?php $n++ ?>
    <?php endforeach; ?>
</div>
