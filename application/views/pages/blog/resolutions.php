<div class="row drain" id="about">
    <h1 class="text-center">11 Easy Plumbing System Resolutions For 2019<br>That You Can Start Today</h1>
</div>

<div class="row sidebar" id="form">
    <div class="col-sm-12 col-md-4">
        <img src="/assets/images/teamphoto-2018.png?1522940306" class="img-rounded img-responsive thumbnail center-block" />
        <p><b>If you need responsive plumbing service</b>, want to discuss a project, request a quote or chat about a recent service call, please contact American Plumbing Company. </p>

        <div class="break"></div>

        <h4>Contact Information</h4>
        <p><a href="tel:9035925508">(903) 592-5508</a> – Office<br/>
			(903) 592-6055 – Fax<br/>
			<a href="tel:9037801512">(903) 780-1512</a> – 24-Hour Emergency Service</p>

		<p> Customer Service: <a href="mailto:cs@apctyler.com">cs@apctyler.com</a><br/>
			Human Resources: <a href="mailto:hr@apctyler.com">hr@apctyler.com</a></p>

		<div class="break"></div>

        <h4>Business Hours</h4>
        <p><b>Monday - Friday:</b> 7:30am - 4:30pm</p>

		<div class="break"></div>

        <h4>New Location</h4>
        <p>We've moved and our new locations is:<br/>
	        1620 North Parkdale<br/>
	        Tyler, TX  75702</p>
    </div>

    <div class="col-sm-12 col-md-8 form">
        <img src="" alt="11 Easy Plumbing System Resolutions For 2019 That You Can Start Today">
        <p>When making New Year’s Resolutions, most homeowners don’t give their home’s plumbing system a second thought. Perhaps your resolution is to save money and become healthier.  Well, taking care of your plumbing system, can help with both of those New Year’s Resolutions.</p>

        <p>Most people don’t think about their home’s plumbing system until there is an emergency or until they realize the small, yet annoying, plumbing issue has turned into a major problem. Unfortunately, in most cases, the longer a problem exists, the more costly the repair. But if you do regular plumbing maintenance, it can help you avoid a lot of common plumbing problems.</p>

        <p>Routine inspections, preventative maintenance,  drain cleaning, and a few inexpensive (or free) safeguards will help to maintain your plumbing system’s efficiency, reduce cost, and improve the quality and safety of your water.</p>

        <h2>Locate Your Shut Off Valve Locations</h2>
        <p>Know how to shut if off!  The water heater, toilet, sink, and washing machine each have their own water shut off valve.  It’s important that you locate where the valves are for shutting off the water supply to each of these items. You should also know the location of the main, shut-off water valve for the home.  In an emergency situation, you don’t want to waste time searching for a valve.  Remember, an ounce of prevention...</p>

        <h2>Schedule Regular Check Ups For Your Plumbing System</h2>
        <p>Contact a professional plumber about scheduling regular system maintenance.  Regular maintenance for your water heater, sewer, and other plumbing system items will help keep your systems running efficiently.  Many plumbing companies offer an annual maintenance program, so be sure and inquire about any cost savings programs that may be available.</p>

        <p>It is also a good idea to have the sewer lines snaked via the clean-out of your home at least every two years.  Snaking the lines will help ensure that your pipes are kept free of buildup and clogs.</p>

        <h2>Learn What Can Be Put Down the Garbage Disposal</h2>
        <p>Respect the garbage disposal.  Remember, your garbage disposal is not meant to be used as a trash can.  Only certain things can safely be put into the disposal. Your owner’s manual is the best source of do’s and don’ts, but obviously, you shouldn’t pour grease or fat, or drop coffee grounds, rice, pasta, eggshells, bones, or stringy vegetables, such as celery, into the disposal. Make sure all family members understand what’s allowed and what is not.</p>

        <p>Don’t forget to clean your garbage disposal regularly to avoid the sour smells that can occasionally develop.  Simply pouring a mixture of warm water, vinegar, and a bit of lemon juice can help reduce any odors from the garbage disposal.</p>

        <h2>Don’t Waste as Much Water</h2>
        <p>Make a pledge to save water.  Not only will you be helping conserve one of our most important natural resources, but you will also see the savings in the amount of your water bill.  Reducing water waste is a win-win situation and a good resolution to work hard at each day. Simple solutions such as turning off your faucet and installing faucets and toilets that are energy efficient can help you achieve your resolution this year and years to come. Not only can these easy, water saving solutions help with lowering water bills, but they will also help with protecting our environmental resources.</p>

        <h2>Schedule Water Heater Maintenance</h2>
        <p>Staying in hot water?  When was the last time you had a plumber check out your hot water heater?  Sediment and build up will collect over time, so having your hot water heater flushed by a professional plumber will not only help protect the system and pipes, but will also help with the water quality and hot water efficiency in your home.  Furthermore,  flushing your water heater once a year or every six months helps reduce your home's energy use.   Faster delivery of hot water and reduced energy usage add up to a lower monthly utility bill.</p>

        <h2>Warm Up Your Pipes</h2>
        <p>Make sure that you’re warming up your pipes.  When the weather turns cold in East Texas, one of the biggest problems that people have is pipes freezing.  Unfortunately, many end up with the inconvenience and costly repairs for a burst pipe.  Remember, it’s not simply the pipe that gets damaged.   Fortunately, there is an easy way to protect your pipes during the frigid winter.  Wrap any exposed pipes with insulation, which can be purchased at any hardware store.  This will help keep your pipes from freezing and bursting. For added protection, you also can consult with a plumber to find out about winterizing your pipes.</p>

        <h2>Install a Water Filter or Water Softener</h2>
        <p>Keep your water clean and clear.   We are fortunate to have clean, drinkable water in Tyler and throughout East Texas. However, there are still things that should be filtered from your water.  Dirt, sediment, unhealthy chemicals, and sand are often found in water.   A plumber can help you with options for your particular needs.  If you are looking for something that can separate the sand and help with keeping your faucets, water heater and drains sediment free or if you simply want to reduce the chlorine in your shower, you will have the added benefit of water that tastes much better and is healthier for you.</p>

        <p>Furthermore, if  you notice  that your hair, hands, and skin are becoming dry, it could be just the cold weather, or it could be your water.  Have your water tested; simple water testing kits can be found at Lowes, Home Depot, Walmart, and the like.  If  you have hard water, install a water softener or water filtration system to reduce the minerals and deposits in the water.</p>

        <h2>Be Mindful of What Goes in Your Toilet</h2>
        <p>Remember the purpose of your toilet!  There are only certain things that should go into your toilet – toilet paper and human waste. Everything else should go into a trashcan.  Paper towels,  wet wipes, hair, tissues, cotton swabs, and sanitary supplies should never be placed in a toilet. Serious clogs that can cause serious problems can occur when any items other than toilet paper and human waste are put into your toilet.  Be mindful of what is being flushed down your toilet.</p>

        <h2>Get Guards for Your Drain</h2>
        <p>Make a trip to the Dollar Store or Home Depot to purchase cheap, drain guards.   Taking the time to spend a few dollars up front could end up saving you a lot more money in the future on costly damage caused by drain clogs.  This easy solution will help guard against hair and other potentially damaging items from entering your pipes.  Remember, our ounce of prevention theory?</p>

        <h2>Promise to Fix Plumbing Problems Right Away</h2>
        <p>Resolve to address any plumbing issues as soon as you you notice a problem.  One major mistake that homeowners make when it comes to their plumbing is that they put plumbing problems off. If you noticed sediment, unusual noises, a continuously running toilet or leaks, don’t put it off!  Plumbing problems do not resolve on their own.  Keep the phone number of your plumbing company handy and immediately call, so the issues can be addressed sooner rather than later.</p>

        <h2>Have a Reliable Plumber or Plumbing Company That You Trust</h2>
        <p>Lucky you, if you have never had to call a plumber.  However, chances are that you will need one at some point.  Research the best plumbers and plumbing companies in Tyler or East Texas ahead of time and know who to call when you need them.  Be sure and make note of after hours emergency numbers.</p>

        <p>So, there you go.  11 easy ways to keep your plumbing system healthy and running smoothly in 2019.  Finally, resolutions that are easy to keep and will pay off in many ways throughout the years.</p>
    </div>
</div>