<div class="row drain" id="about">
    <h1 class="text-center">Online Employment Application <span>Become part of the APC Family</span></h1>
</div>

<div class="row sidebar" id="form">
    <div class="col-sm-12 col-md-4">
        <img src="/assets/images/teamphoto-2018.png?1522940306" class="img-rounded img-responsive thumbnail center-block" />
        <p><b>If you need responsive plumbing service</b>, want to discuss a project, request a quote or chat about a recent service call, please contact American Plumbing Company. </p>

        <div class="break"></div>

        <h4>Contact Information</h4>
        <p><a href="tel:9035925508">(903) 592-5508</a> – Office<br/>
			(903) 592-6055 – Fax<br/>
			<a href="tel:9037801512">(903) 780-1512</a> – 24-Hour Emergency Service</p>
			
		<p> Customer Service: <a href="mailto:cs@apctyler.com">cs@apctyler.com</a><br/>
			Human Resources: <a href="mailto:hr@apctyler.com">hr@apctyler.com</a></p>
        
		<div class="break"></div>

        <h4>Business Hours</h4>
        <p><b>Monday - Thursday:</b> 7:30am - 4:30pm<br/>
        	<b>Friday:</b> 7:30am - 3:00pm</p>
    </div>

    <div class="col-sm-12 col-md-8 form">
	    <h3>You Will Be PROUD To Be an American Associate!</h3>
		<p>At American Plumbing Company, we take pride in our "Satisfaction Guaranteed" 
			commitment to our growing customer base.</p>
		<p>Because we care about our community, and our profession, we are always looking 
			for individuals of strong character and integrity to add to our team.</p>
		<p>We offer a stable, friendly, team-oriented work environment, and provide the 
			compensation and benefits you need to care for your family.</p>
	    
        <?php if($formsubmit == FALSE): ?>

            <?php if(validation_errors()): ?>
                <div class="alert alert-danger" role="alert"><?=validation_errors(); ?></div>
            <?php endif; ?>

            <?=form_open('contact/employment'); ?>
                <div class="form-group">
                    <label for="fullname">Your Name</label>
                    <input type="text" name="fullname" class="form-control" id="fullname" placeholder="Your Full Name" value="<?=set_value('fullname'); ?>">
                </div>
                
                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea id="address" name="address" class="form-control" rows="2"><?=set_value('address'); ?></textarea>
                </div>

                <div class="row">
                    <div class="col-md-6">
		                <div class="form-group">
		                    <label for="city">City</label>
		                    <input type="text" name="city" class="form-control" id="city" placeholder="City" value="<?=set_value('city'); ?>">
		                </div>
                    </div>
                    
                    <div class="col-md-2">
		                <div class="form-group">
		                    <label for="state">State</label>
		                    <select class="form-control" name="state">
								<option value="AL">AL</option>
								<option value="AK">AK</option>
								<option value="AZ">AZ</option>
								<option value="AR">AR</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DE">DE</option>
								<option value="DC">DC</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="IA">IA</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="ME">ME</option>
								<option value="MD">MD</option>
								<option value="MA">MA</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MS">MS</option>
								<option value="MO">MO</option>
								<option value="MT">MT</option>
								<option value="NE">NE</option>
								<option value="NV">NV</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NY">NY</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX" selected>TX</option>
								<option value="UT">UT</option>
								<option value="VT">VT</option>
								<option value="VA">VA</option>
								<option value="WA">WA</option>
								<option value="WV">WV</option>
								<option value="WI">WI</option>
								<option value="WY">WY</option>
							</select>
		                </div>
                    </div>
                    
                    <div class="col-md-4">
		                <div class="form-group">
		                    <label for="zip">Zip Code</label>
		                    <input type="text" name="zip" class="form-control" id="zip" placeholder="Zip Code" value="<?=set_value('zip'); ?>">
		                </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Your Email Address" value="<?=set_value('email'); ?>">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                <input id="phone" name="phone" type="text" class="form-control" placeholder="(903) 555-5555" value="<?=set_value('phone'); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="license_num">Plumber's License #</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-wrench"></i></div>
                                <input type="text" name="license_num" class="form-control" id="license_num" placeholder="Your Plumber's License Number" value="<?=set_value('email'); ?>">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
		                    <label for="education">Level of Education</label>
		                    <select class="form-control" name="education">
								<option selected="selected">Select one</option>
								<option value="Grade School">Grade School</option>
								<option value="Middle/Junior High School">Middle/Junior High School</option>
								<option value="High School">High School</option>
								<option value="College">College</option>
								<option value="Graduate/Professional School">Graduate/Professional School</option>
							</select>
		                </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
		                <div class="form-group">
		                    <label for="position_of_interest">Position(s) of Interest</label>
		                    <textarea id="message" name="position_of_interest" class="form-control" rows="2"><?=set_value('position_of_interest'); ?></textarea>
		                </div>
                    </div>

                    <div class="col-md-6">
		                <div class="form-group">
		                    <label for="date_avaliable">Date Available to Start</label>
		                    <input type="text" name="date_avaliable" class="form-control" id="zip" placeholder="Date Avaliable" value="<?=set_value('date_avaliable'); ?>">
		                </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="skills">Skills</label>
                    <textarea id="skills" name="skills" class="form-control" rows="6"><?=set_value('skills'); ?></textarea>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6Lca-wETAAAAAPylyVRwKMHs_nALwER2qwmTbVRj"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>
                </div>

            <?=form_close(); ?>

        <?php else: ?>
            <h3>Thank you for contacting American Plumbing Company.</h3>
            <p>One of our customer service representatives will be in touch with you within the next business day to discuss your message.</p>

        <?php endif; ?>
    </div>
</div>