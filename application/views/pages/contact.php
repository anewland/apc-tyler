<div class="row drain" id="about">
    <h1 class="text-center">Contacting American Plumbing Co <span>Calling a plumber has never been so simple</span></h1>
</div>

<div class="row sidebar" id="form">
    <div class="col-sm-12 col-md-4">
        <img src="/assets/images/teamphoto-2018.png?1522940306" class="img-rounded img-responsive thumbnail center-block" />
        <p><b>If you need responsive plumbing service</b>, want to discuss a project, request a quote or chat about a recent service call, please contact American Plumbing Company. </p>

        <div class="break"></div>

        <h4>Contact Information</h4>
        <p><a href="tel:9035925508">(903) 592-5508</a> – Office<br/>
			(903) 592-6055 – Fax<br/>
			<a href="tel:9037801512">(903) 780-1512</a> – 24-Hour Emergency Service</p>
			
		<p> Customer Service: <a href="mailto:cs@apctyler.com">cs@apctyler.com</a><br/>
			Human Resources: <a href="mailto:hr@apctyler.com">hr@apctyler.com</a></p>
        
		<div class="break"></div>

        <h4>Business Hours</h4>
        <p><b>Monday - Friday:</b> 7:30am - 4:30pm</p>

		<div class="break"></div>

        <h4>New Location</h4>
        <p>We've moved and our new locations is:<br/>
	        1620 North Parkdale<br/>
	        Tyler, TX  75702</p>
    </div>

    <div class="col-sm-12 col-md-8 form">
        <?php if($formsubmit == FALSE): ?>

            <?php if(validation_errors()): ?>
                <div class="alert alert-danger" role="alert"><?=validation_errors(); ?></div>
            <?php endif; ?>

            <?=form_open('contact'); ?>
                <div class="form-group">
                    <label for="fullname">Your Name</label>
                    <input type="text" name="fullname" class="form-control" id="fullname" placeholder="Your Full Name" value="<?=set_value('fullname'); ?>">
                </div>

                <div class="form-group">
                    <label for="company">Company</label>
                    <input type="text" name="company" class="form-control" id="company" placeholder="Your Company Name" value="<?=set_value('company'); ?>">
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Your Email Address" value="<?=set_value('email'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                <input id="phone" name="phone" type="text" class="form-control" placeholder="(903) 555-5555" value="<?=set_value('phone'); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea id="message" name="message" class="form-control" rows="6"><?=set_value('message'); ?></textarea>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6Lca-wETAAAAAPylyVRwKMHs_nALwER2qwmTbVRj"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>
                </div>

            <?=form_close(); ?>

        <?php else: ?>
            <h3>Thank you for contacting American Plumbing Company.</h3>
            <p>One of our customer service representatives will be in touch with you within the next business day to discuss your message.</p>

        <?php endif; ?>
    </div>
</div>