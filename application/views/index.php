<div class="row" id="services">
    <h1 class="text-center">Professional Plumbing Services <span>Our personal touch to professional plumbing</span></h1>

    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services/">
            <img src="/assets/images/service/plumbingrepairs.jpg" class="img-responsive" />
            <div><span>Plumbing Repairs <br/>&amp; Replacement</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services/">
            <img src="/assets/images/service/waterheater.jpg" class="img-responsive" />
            <div><span>Water Heater <br/>Repair &amp; Replacement</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services/">
            <img src="/assets/images/service/garbagedisposal.jpg" class="img-responsive" />
            <div><span>Garbage Disposal <br/>Repair &amp; Replacement</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services/">
            <img src="/assets/images/service/sewer.jpg" class="img-responsive" />
            <div><span>Sewer &amp; Drain <br/>Cleaning</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services/">
            <img src="/assets/images/service/gasline.jpg" class="img-responsive" />
            <div class="uno"><span>Gas Line Services</span></div>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 service-area">
        <a href="/services/">
            <img src="/assets/images/service/commercial.jpg" class="img-responsive" />
            <div class="uno"><span>Commercial Services</span></div>
        </a>
    </div>

</div> <!-- /.services -->


<div class="row" id="testimonials">
    <h1 class="text-center">Why Call American Plumbing Company? <span>Don't just take our word for it, see what others are saying about us.</span></h1>

    <?php foreach($testimonials as $t):?>
        <div class="col-md-6 col-xs-12 testi">
            <div>
            	<p class="testimonial"><?=$t['quote']?></p>
            	<p class="person">
	            	<?=$t['person']?>
            		<?php if($t['company'] != NULL): ?>from <?=$t['company']?> <?php endif; ?>
            		in <?=$t['location']?>
            	</p>
            </div>
        </div>
    <?php endforeach; ?>
</div> <!-- /.testimonials -->
