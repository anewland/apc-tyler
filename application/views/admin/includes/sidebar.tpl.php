<div id="main-menu">
    <ul class="sidebar-nav">
        <li class='<?=($active == 'dashboard' ? 'current' : ''); ?> hidden'>
        	<a href='/admin/dashboard'><i class='fa fa-dashboard'></i><span class='sidebar-text'>Dashboard</span></a>
        </li>
        <li class='<?=($active == 'employee' ? 'current' : ''); ?>'>
        	<a href='/admin/employees'><i class='fa fa-users'></i><span class='sidebar-text'>Employees</span></a>
        </li>
        <li class='<?=($active == 'faqs' ? 'current' : ''); ?>'>
        	<a href='/admin/faqs'><i class='fa fa-question-circle'></i><span class='sidebar-text'>FAQs</span></a>
        </li>
        <li class='<?=($active == 'testimonials' ? 'current' : ''); ?>'>
        	<a href='/admin/testimonials'><i class='fa fa-envelope-square'></i><span class='sidebar-text'>Client Testimonials</span></a>
        </li>
        <li class='<?=($active == 'quotes' ? 'current' : ''); ?>'>
        	<a href='/admin/quotes'><i class='fa fa-usd'></i><span class='sidebar-text'>Quote/Service Requests</span></a>
        </li>
        <li class='<?=($active == 'applicants' ? 'current' : ''); ?>'>
        	<a href='/admin/applicants'><i class='fa fa-archive'></i><span class='sidebar-text'>APC Applicants</span></a>
        </li>
    </ul>
</div>
