    </div>
    <!-- END WRAPPER -->
    
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src='/assets/admin/plugins/mandatoryJs.min.js'></script>
    <!-- END MANDATORY SCRIPTS -->
    
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src='/assets/admin/plugins/bootstrap-switch/bootstrap-switch.js'></script>
    <script src='/assets/admin/plugins/metrojs/metrojs.min.js'></script>
    <script src='/assets/admin/plugins/fullcalendar/moment.min.js'></script>
    <script src='/assets/admin/plugins/fullcalendar/fullcalendar.min.js'></script>
    <script src='/assets/admin/plugins/simple-weather/jquery.simpleWeather.min.js'></script>
    <script src='/assets/admin/plugins/google-maps/markerclusterer.js'></script>
    <script src='http://maps.google.com/maps/api/js?sensor=true'></script>
    <script src='/assets/admin/plugins/google-maps/gmaps.js'></script>
    <script src='/assets/admin/plugins/charts-flot/jquery.flot.js'></script>
    <script src='/assets/admin/plugins/charts-flot/jquery.flot.animator.min.js'></script>
    <script src='/assets/admin/plugins/charts-flot/jquery.flot.resize.js'></script>
    <script src='/assets/admin/plugins/charts-flot/jquery.flot.time.min.js'></script>
    <script src='/assets/admin/plugins/charts-morris/raphael.min.js'></script>
    <script src='/assets/admin/plugins/charts-morris/morris.min.js'></script>
    <script src='/assets/admin/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>
    <script src='/assets/admin/js/calendar.js'></script>
    <script src='/assets/admin/js/dashboard.js'></script>
    <script src='/assets/admin/js/custom.js'></script>
    <!-- END  PAGE LEVEL SCRIPTS -->
    
    <script src='/assets/admin/js/application.js'></script>
</body>

</html>