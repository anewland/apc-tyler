<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"><!--<![endif]-->

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>APC Tyler | Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name"robots" content="noindex, nofollow">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="/assets/images/icons/favicon.ico">
    <!-- END META SECTION -->
    
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/assets/admin/css/icons/icons.min.css" rel="stylesheet">
    <link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/admin/css/plugins.css" rel="stylesheet">
    <link href="/assets/admin/css/style.min.css" rel="stylesheet">
    <link href="/assets/admin/css/custom.css" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    
    <!-- BEGIN PAGE LEVEL STYLE -->
    <link href="/assets/admin/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="/assets/admin/plugins/metrojs/metrojs.css" rel="stylesheet">
    <!-- END PAGE LEVEL STYLE -->
    
    <script src="/assets/admin/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body data-page="dashboard">
	
    <!-- BEGIN TOP MENU -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="menu-medium" class="sidebar-toggle tooltips">
                    <i class="fa fa-outdent"></i>
                </a>
                <a class="navbar-brand" href="/"></a>
            </div>
            <div class="navbar-center">APC Tyler | Dashboard</div>
        </div>
    </nav>
    <!-- END TOP MENU -->
    
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
	    
        <!-- BEGIN MAIN SIDEBAR -->
        <nav id="sidebar">
			<?php include 'sidebar.tpl.php'; ?>
            
           <?php include 'quick-analytics.tpl.php'; ?>
            
        </nav>
        <!-- END MAIN SIDEBAR -->
