<div class="footer-widget">
	<div class="footer-gradient"></div>
	<div id="sidebar-charts">
	    <div class="sidebar-charts-inner">
	        <div class="sidebar-charts-left">
	            <div class="sidebar-chart-title">Monthly Pageviews</div>
	            <div class="sidebar-chart-number">1,256</div>
	        </div>
	        <div class="sidebar-charts-right" data-type="bar" data-color="theme">
	            <span class="dynamicbar1"></span>
	        </div>
	    </div>
	    <hr class="divider">
	    <div class="sidebar-charts-inner">
	        <div class="sidebar-charts-left">
	            <div class="sidebar-chart-title">Quarterly Pageviews</div>
	            <div class="sidebar-chart-number">47,564</div>
	        </div>
	        <div class="sidebar-charts-right" data-type="bar" data-color="theme">
	            <span class="dynamicbar2"></span>
	        </div>
	    </div>
	    <hr class="divider">
	    <div class="sidebar-charts-inner">
	        <div class="sidebar-charts-left">
	            <div class="sidebar-chart-title">Yearly Pageviews</div>
	            <div class="sidebar-chart-number" id="number-visits">147,687</div>
	        </div>
	        <div class="sidebar-charts-right" data-type="bar" data-color="theme">
	            <span class="dynamicbar3"></span>
	        </div>
	    </div>
	</div>
</div>