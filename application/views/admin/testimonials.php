<!-- BEGIN MAIN CONTENT -->
	<div id="main-content" class="dashboard">
    	<div class="top-page clearfix">
            <div class="page-title pull-left">
                <h3 class="pull-left"><strong>Client Testimonials</strong></h3>
            </div>
             <div class="pull-right">
                <a href="/admin/testimonial_add" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> New Testimonial</a>
            </div>
        </div>
		
            
    </div>
<!-- END MAIN CONTENT -->