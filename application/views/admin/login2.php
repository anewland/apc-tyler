<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"><!--<![endif]-->

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>APC Tyler | Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name"robots" content="noindex, nofollow">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="/assets/images/icons/favicon.ico">
    <!-- END META SECTION -->
    
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/assets/admin/css/icons/icons.min.css" rel="stylesheet">
    <link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/admin/css/plugins.css" rel="stylesheet">
    <link href="/assets/admin/css/style.min.css" rel="stylesheet">
    <link href="/assets/admin/css/custom.css" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    
    <!-- BEGIN PAGE LEVEL STYLE -->
    <link href="/assets/admin/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="/assets/admin/plugins/metrojs/metrojs.css" rel="stylesheet">
    <!-- END PAGE LEVEL STYLE -->
    
    <script src="/assets/admin/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body class="login fade-in" data-page="login">
    <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="login-logo">
                        <a href="#?login-theme-3">
                            <img src="/assets/admin/img/account/login-logo.png" alt="Company Logo">
                        </a>
                    </div>
                    <hr>
                    <div class="login-form">
                        <!-- BEGIN ERROR BOX -->
                        <div class="alert alert-danger hide">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <h4>Error!</h4>
                            Your Error Message goes here
                        </div>
                        <!-- END ERROR BOX -->
                        <form action="#" method="post">
                            <input type="text" placeholder="Username" class="input-field form-control user" />
                            <input type="password" placeholder="Password" class="input-field form-control password" />
                            <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left"><span class="ladda-label">login</span></button>
                        </form>
                        <div class="login-links">
                            <a href="password_forgot.html">Forgot password?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END LOCKSCREEN BOX -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="/assets/admin/plugins/jquery-1.11.js"></script>
    <script src="/assets/admin/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="/assets/admin/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="/assets/admin/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="/assets/admin/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/admin/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!-- END MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/admin/plugins/backstretch/backstretch.min.js"></script>
    <script src="/assets/admin/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="/assets/admin/js/account.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

</body>

</html>
