<!-- BEGIN MAIN CONTENT -->
	<div id="main-content" class="dashboard">
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3><strong>APC Tyler</strong> | 
	            <?php if($employee): ?>
	            	<?=$employee->first_name. ' ' .$employee->last_name?>
	            <?php else: ?>
	            	New Employee
            	<?php endif; ?>
	        </h3>
        </div>
		
	    <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span8">
	                            <?php if($employee): ?>
	                            	<form id="employeeUpdate" method="post" action="/admin/employee_update/<?=$employee->eid?>">
								<?php else: ?>
	                            	<form id="employeeUpdate" method="post" action="/admin/employee_addition">
								<?php endif; ?>

	                            	<div class="row">
		                            	<div class="form-group col-md-10">
		                                    <label class="form-label"><strong>First Name</strong></label>
		                                    <span class="tips"> required</span>
		                                    <div class="controls">
		                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->first_name; }?>" id="first_name" name="first_name">
		                                    </div>
		                            	</div>
		                            	
		                            	<div class="form-group col-md-2">
		                                    <label class="form-label"><strong>Sort Order</strong></label>
		                                    <div class="controls">
		                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->sort; }?>" id="sprt" name="sort">
		                                    </div>
		                            	</div>
	                                </div>
	
									<div class="row">
		                            	<div class="form-group col-md-10">
		                                    <label class="form-label"><strong>Last Name</strong></label>
		                                    <span class="tips"> required</span>
		                                    <div class="controls">
		                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->last_name; }?>" id="last_name" name="last_name">
		                                    </div>
		                                </div>
		                            	
		                            	<div class="form-group col-md-2">
		                                    <label class="form-label"><strong>Active</strong></label>
		                                    <div class="controls">
		                                        <input type="checkbox" class="switch" <?php if($employee) {echo ($employee->visible == 1 ? 'checked' : '');} ?> id="visible" name="visible" data-size="small">
		                                    </div>
		                            	</div>
	                                </div>
	
	
	
	                            	<div class="form-group">
	                                    <label class="form-label"><strong>Title</strong></label>
	                                    <span class="tips"> required</span>
	                                    <div class="controls">
	                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->title; }?>" id="title" name="title">
	                                    </div>
	                                </div>
	
	                            	<div class="row">
		                            	<div class="form-group col-md-8">
		                                    <label class="form-label"><strong>Email Address</strong></label>
		                                    <div class="controls">
		                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->email; }?>" id="email" name="email">
		                                    </div>
		                                </div>
		                                
		                                
		                            	<div class="form-group col-md-4">
		                                    <label class="form-label"><strong>Hire Date</strong></label>
											<span class="tips"> YYYY-MM-DD format</span>
		                                    <div class="controls">
		                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->employment_year; }?>" id="employment_year" name="employment_year">
		                                    </div>
		                                </div>
	                            	</div>
	                                
	                                <div class="form-group">
	                                    <label class="form-label"><strong>Bio</strong></label>
	                                    <div class="controls">
	                                        <textarea class="form-control" rows="6" id="bio" name="bio"><?php if($employee) { echo $employee->bio; }?></textarea>
	                                    </div>
	                                </div>
	                                
	                                <hr />
	                                
	                            	<div class="form-group">
	                                    <label class="form-label"><strong>Photo URL</strong></label>
	                                    <div class="controls">
	                                        <input type="text" class="form-control" value="<?php if($employee) { echo $employee->photo; }?>" id="photo" name="photo">
	                                    </div>
	                                </div>
                                
	                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row-fluid">
	                        <?php if($employee): ?>
                            	<img src="/assets/images/employees/<?=$employee->photo?>" class="img-responsive img-rounded" style="width:100%"><br/>
                            	<img src="/assets/images/employees/lg/<?=$employee->photo?>" class="img-responsive hidden img-rounded" style="width:100%">
							<?php else: ?>
								<img src="/assets/images/employees/default.jpg" class="img-responsive img-rounded" style="width:100%">
							<?php endif; ?>
                        </div>
                    </div>
                </div>
                
				<div class="pull-right">
                    <button onclick="employeeUpdate()" type="submit" class="btn btn-primary m-b-10">Update</button>
                    <a href="/admin/employees" class="btn btn-default m-b-10">Cancel</a>
                </div>
            </div>
        </div>

		<pre><?=print_r($employee);?></pre>
            
    </div>
<!-- END MAIN CONTENT -->