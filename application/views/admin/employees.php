<!-- BEGIN MAIN CONTENT -->
	<div id="main-content" class="dashboard">
    	<div class="top-page clearfix">
            <div class="page-title pull-left">
                <h3 class="pull-left"><strong>Employees</strong></h3>
            </div>
             <div class="pull-right">
                <a href="/admin/employee_add" class="btn btn-success m-t-10"><i class="fa fa-plus p-r-10"></i> New Employee</a>
            </div>
        </div>
		
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead class="no-bd">
                                            <tr>
                                                <th></th>
                                                <th><strong>Photo</strong></th>
                                                <th><strong>Name</strong></th>
                                                <th><strong>Email Address</strong></th>
                                                <th><strong>Hire Date</strong></th>
                                                <th><strong>Bio</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-bd-y">
	                                        <?php foreach($employees as $e):?>
                                            <tr class='<?=($e['visible'] == '0' ? 'text-muted danger' : ''); ?>'>
	                                            <td><i class="fa fa-bars"></i></td>
                                                <td>
	                                                <?php if($e['photo']):?>
	                                                	<img src="/assets/images/employees/<?=$e['photo']?>" class="img-responsive img-rounded" style="width:120px;">
	                                                <?php else: ?>
	                                                	<img src="/assets/images/employees/default.jpg" class="img-responsive img-rounded" style="width:120px;">
	                                                <?php endif; ?>
	                                            </td>
                                                <td><p class="lead"><a href="/admin/employee_edit/<?=$e['eid']?>"><?=$e['first_name']?> <?=$e['last_name']?></a><br/><small><?=$e['title']?></small></p></td>
                                                <td><?=$e['email']?></td>
                                                <td><?=$e['employment_year']?></td>
                                                <td><?=mb_strimwidth($e['bio'], 0, 50, '...')?></td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    </div>
<!-- END MAIN CONTENT -->