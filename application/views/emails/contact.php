<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this meta tag, Half Life 3 will never be released. -->
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>American Plumbing Company | Contact Form</title>
    <link rel="stylesheet" type="text/css" href="http://apctyler.com/assets/css/email.css" />
</head>
<body bgcolor="#FFFFFF">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#f0f0f0">
    <tr>
        <td></td>
        <td class="header container" >
            <div class="content">
                <table bgcolor="#f0f0f0">
                    <tr>
                        <td><img src="http://apctyler.com/assets/images/branding.png" /></td>
                        <td align="right"><h6 class="collapse">Contact Form</h6></td>
                    </tr>
                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->

<!-- BODY -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <div class="content">
                <table>
                    <tr>
                        <td>
                            <h3><?=$greeting?>, <?=$emaildata['fullname']?></h3>
                            <p class="lead"><b>Thank you for contacting American Plumbing Co.</b></p>
                            <p>One of our customer service representatives will be in touch with you within the next business day to discuss your message.</p>
                            <br /><br />
                                <p class="lead">Quote Information</p>
                                <p class="callout"><b>Name:</b> <?=$emaildata['fullname']?><br/>
                                    <?php if($emaildata['company']):?>
                                        <b>Company:</b> <?=$emaildata['company']?><br/>
                                    <?php endif; ?>
                                    <b>Email Address:</b> <?=$emaildata['email']?><br/>
                                    <b>Phone Number:</b> <?=$emaildata['phone']?><br/>
                                    <b>Message:</b> <?=$emaildata['message']?></p>

                            <!-- social & contact -->
                            <table class="social" width="100%">
                                <tr>
                                    <td>

                                        <!-- column 1 -->
                                        <table align="left" class="column">
                                            <tr>
                                                <td>

                                                    <h5 class="">Connect with Us:</h5>
                                                    <p class=""><a href="https://www.facebook.com/pages/American-Plumbing-Company-Inc/183224941709168" class="fb" style="background-color:#3B5998;padding: 3px 7px;font-size: 12px;margin-bottom: 10px;text-decoration: none;color: #FFF;font-weight: bold;display: block;text-align: center;">Facebook</a></p>
                                                </td>
                                            </tr>
                                        </table><!-- /column 1 -->

                                        <!-- column 2 -->
                                        <table align="left" class="column">
                                            <tr>
                                                <td>
                                                    <h5 class="">Contact Info:</h5>
                                                    <p>Phone: <strong>(903) 592-5508</strong><br/>
                                                        Email: <strong><a href="emailto:cs@apctyler.com">cs@apctyler.com</a></strong></p>
                                                </td>
                                            </tr>
                                        </table><!-- /column 2 -->

                                        <span class="clear"></span>

                                    </td>
                                </tr>
                            </table><!-- /social & contact -->

                        </td>
                    </tr>
                </table>
            </div><!-- /content -->
        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

</body>
</html>