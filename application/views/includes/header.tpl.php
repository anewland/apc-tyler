<?php
	$dayphone = '903.592.5508';
	$nightphone = '903.780.1512';

	$n = date('w').' dow, '.date('Gi').' time';
	echo '<!-- '.$n. ' -->';

	// If the day is Monday (1) - Friday (5)
	if(date('w') >= '1' && date('w') <= '5') {
		// If its after 7:30a (730) but before 5:30pm (1730))
		if(date('Gi') >= '730' && date('Gi') <= '1730') {
			$phone = $dayphone;
		} else {
			$phone = $nightphone;
		}
	} else {
		$phone = $nightphone;
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<!-- Meta Tage -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<title>American Plumbing Company | <?=$pageTitle?></title>
	<meta name="google-site-verification" content="9S7obiAdFf3_DOEARpA1Scv0lL6llil225upftG-RjA" />

	<!-- Stylesheets and Scripts -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,700,300|Open+Sans+Condensed:300,700|Sansita+One' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/assets/css/mmenu/jquery.mmenu.all.css" />
	<link rel="stylesheet" type="text/css" href="/assets/css/apctyler.css">

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/assets/js/mmenu/jquery.mmenu.min.all.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- iOS web icons -->
    <link rel='shortcut icon' href='/assets/images/icons/favicon.png' type='image/x-icon'/ >
	<link rel="apple-touch-icon" href="/assets/images/icons/touch-icon-iphone.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icons/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icons/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icons/touch-icon-ipad-retina.png">

	<!-- start: Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KHL9DC');</script>
	<!-- end: Google Tag Manager -->
</head>
<body>

	<!-- start: Google Tag Manager -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KHL9DC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- end: Google Tag Manager -->

	<!-- start: Segment Pixel - ETX_Tyler_American Plumbing_RT - DO NOT MODIFY -->
		<script src="https://secure.adnxs.com/seg?add=12488041&t=1" type="text/javascript"></script>
	<!-- end: Segment Pixel -->

	<div class="container">

		<header>
			<div class="header visible-xs">
				<a href="#menu" style="z-index: 99;"></a>
				<a href="tel:<?=$phone?>" class="phonenumber"><i class="fa fa-phone"></i> <?=$phone?></a>
			</div> <!-- /.header .visible-xs-->

			<div class="header-nav clearfix hidden-xs">
				<div class="col-sm-7">

					<ul>
						<li><a href="/" title="American Plumbing Co">Home</a></li>
						<li><a href="/services/" title="APC Plumbing Services">Services</a></li>
						<li><a href="/faqs/" title="Frequently Asked Plumbing Questions">FAQs</a></li>
						<li><a href="/about/" title="All About American Plumbing Co">About</a></li>
						<li><a href="/contact/" title="Contact American Plumbing Co">Contact</a></li>
						<li><a href="/payment/" title="Online Payments">Payment</a></li>
						<li><a href="/contact/employment/" title="Employment">Employment</a></li>
					</ul>
				</div>

				<div class="col-sm-5">
					<a href="tel:<?=$phone?>" class="btn blue-button">
						<p><i class="fa fa-phone"></i> <?=$phone?></p>
					</a>

					<a href="/contact/quote/" class="btn red-button">
						<p><i class="fa fa-clipboard"></i> Request Service</p>
					</a>

					<a href="https://www.facebook.com/apctyler/" class="btn facebook-link hidden-sm">
						<i class="fa fa-facebook-square"></i> <span>Find us on Facebook</span>
					</a>

				</div>
			</div> <!-- /.header-nav .hidden-xs-->

			<div class="branding-bar clearfix">

				<div class="col-md-5 branding">
					<a href="/"><span>American Plumbing Company</span></a>
				</div>

				<div class="col-md-4 col-sm-6 emergency hidden-xs">
					<div>
						<p>24-Hour Emergency Service</p>
						<p class="phone"><?=$phone?></p>
						<i class="fa fa-check fa-3x"></i>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 promotion hidden-xs">
					<div>
						<a href="http://localsloveus.com/tyler/winner/44895/" target="_blank"><img class="img-responsive" src="/assets/images/localsloveus-2017.png" alt="Locals Love Us - Voted Best in Plumbing Contractors 2017-18" /></a>
					</div>
				</div>

			</div> <!-- /.branding-bar -->


			<!-- CAROUSEL -->
			<?php if($slider == TRUE):?>
			<div id="carousel" class="carousel slide">
				<!-- Indicators -->
				<ol class="carousel-indicators hidden">
					<li data-target="#carousel" data-slide-to="0" class="active"></li>
					<li data-target="#carousel" data-slide-to="1"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="/assets/images/slider/mainslider-image.png" alt="...">
						<div class="carousel-caption">
							<h1 class="hidden-sm hidden-xs">We are here for you!</h1>

							<a href="/contact/quote/" class="btn red-button">
								<h3><i class="fa fa-clipboard"></i> Get A Quote <span>or request service</span></h3>
							</a>

							<a href="tel:9035925508" class="btn blue-button hidden-xs">
								<h3><i class="fa fa-phone"></i> <?=$phone?> <span>call us today</span></h3>
							</a>

							<div class="servicing hidden-xs">
								<p>Servicing All of East Texas</p>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /#caroussel -->
			<?php endif;?>

		</header>
