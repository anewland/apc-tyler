		<footer>
			<div class="footer-nav clearfix">
				<div class="col-md-9">
					<ul>
						<li><a href="/" title="American Plumbing Co">Home</a></li>
						<li><a href="/services/" title="APC Plumbing Services">Plumbing Services</a></li>
						<li><a href="/faqs/" title="Frequently Asked Plumbing Questions">FAQs</a></li>
						<li><a href="/about/" title="All About American Plumbing Co">About</a></li>
						<li><a href="/contact/" title="Contact American Plumbing Co">Contact</a></li>
						<li><a href="/payment/" title="Online Payments">Online Payments</a></li>
						<li><a href="/contact/employment/" title="Employment">Employment</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<a href="http://www.bbb.org/east-texas/business-reviews/plumbers/american-plumbing-in-tyler-tx-4000992" target="_blank" class="bbb"><span>American Plumbing Company - BBB Accredited Business since April 1, 1990</span></a>
				</div>
			</div> <!-- /.footer-nav -->

			<div class="copyright">
				<div class="col-md-12">
					<p class="lic">Texas State Plumbing License # M-10861 M-42236| <a href="/privacy-policy/">Privacy Policy</a></p>
					<p class="copy">Copyright &copy; <?=date('Y')?> American Plumbing Company  |  All Rights reserved.</p>
				</div>
			</div> <!-- /.copyright -->

		</footer>

		<nav id="menu">
			<ul>
				<li><a href="/" title="American Plumbing Co">Home</a></li>
				<li><a href="/services/" title="APC Plumbing Services">Plumbing Services</a></li>
				<li><a href="/faqs/" title="Frequently Asked Plumbing Questions">FAQs</a></li>
				<li><a href="/about/" title="All About American Plumbing Co">About</a></li>
				<li><a href="/contact/" title="Contact American Plumbing Co">Contact</a></li>
				<li><a href="/payment/" title="Online Payments">Online Payments</a></li>
				<li><a href="/contact/employment/" title="Employment">Employment</a></li>
			</ul>
		</nav>

	</div> <!-- /.container -->


	<!-- JAVA CALLS -->
	<script type="text/javascript">
	(function(a,e,c,f,g,h,b,d){var k={ak:"853906919",cl:"v2oNCN7N8HoQ56uWlwM",autoreplace:"903.592.5508"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
	</script>
	
	<script type="text/javascript">
		$(function() {
			$('#menu').mmenu();
            $('#carousel').carousel({
	            interval: 2000
            });
		});
	</script>

	<!-- start: Organization Schema Markup -->
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Organization",
	  "name": "American Plumbing Company",
	  "url": "https://www.apctyler.com/",
	  "logo": "https://www.apctyler.com/assets/images/branding.png",
	  "contactPoint": {
	    "@type": "ContactPoint",
	    "telephone": "(903) 592-5508",
	    "contactType": "customer service",
	    "areaServed": "US",
	    "availableLanguage": "English"
	  }
	}
	</script>

	<!-- start: Website Schema Markup -->
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org/",
	  "@type": "WebSite",
	  "name": "American Plumbing Company",
	  "url": "https://www.apctyler.com/"
	}
	</script>

	<!-- start: Local Business Schema Markup -->
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "LocalBusiness",
	  "name": "American Plumbing Company",
	  "image": "https://www.apctyler.com/assets/images/slider/mainslider-image.png",
	  "@id": "https://www.apctyler.com/",
	  "url": "https://www.apctyler.com/",
	  "telephone": "(903) 592-5508",
	  "address": {
	    "@type": "PostalAddress",
	    "streetAddress": "1620 North Parkdale",
	    "addressLocality": "Tyler",
	    "addressRegion": "TX",
	    "postalCode": "75702",
	    "addressCountry": "US"
	  },
	  "geo": {
	    "@type": "GeoCoordinates",
	    "latitude": 32.3668842,
	    "longitude": -95.32328040000004
	  },
	  "openingHoursSpecification": {
	    "@type": "OpeningHoursSpecification",
	    "dayOfWeek": [
	      "Monday",
	      "Tuesday",
	      "Wednesday",
	      "Thursday",
	      "Friday"
	    ],
	    "opens": "07:30",
	    "closes": "16:30"
	  }
	}
	</script>
	<!-- end: Schema Markups -->

</body>
</html>
