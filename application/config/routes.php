<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['about'] = 'info';
$route['privacy-policy'] = 'info/privacy_policy';

$route['about/milton-vanderpool'] = 'info/member/1';
$route['about/lori-townsend'] = 'info/member/3';
$route['about/brad-townsend'] = 'info/member/4';
$route['about/danny-pinkerton'] = 'info/member/5';
$route['about/daniel-victory'] = 'info/member/8';
$route['about/ryan-fisher'] = 'info/member/9';
$route['about/troey-grimes'] = 'info/member/11';
$route['about/tim-baskin'] = 'info/member/13';
$route['about/matt-nice'] = 'info/member/14';
$route['about/josh-washington'] = 'info/member/15';
$route['about/jonny-lowe'] = 'info/member/17';
$route['about/thomas-davis'] = 'info/member/21';
$route['about/kevin-brown'] = 'info/member/23';
$route['about/jose-castillo'] = 'info/member/24';
$route['about/kyle-davis'] = 'info/member/26';
$route['about/david-perez'] = 'info/member/27';
$route['about/casey-torres'] = 'info/member/28';
$route['about/jd-castillo'] = 'info/member/29';
$route['about/dillon-grimes'] = 'info/member/30';
$route['about/pablo-perez'] = 'info/member/31';

// PREVIOUS WORKERS
$route['about/todd-tallant'] = 'info/member/10';
$route['about/rick-guerra'] = 'info/member/16';
$route['about/melvin-garay-reyes'] = 'info/member/18';
$route['about/colin-moore'] = 'info/member/19';
$route['about/dave-howell'] = 'info/member/2';
$route['about/dan-harner'] = 'info/member/25';
$route['about/ricardo-guerra'] = 'info/member/7';

// SERVICES
$route['services/water-leak-repair'] = 'services/water_leak_repair';
$route['services/gas-leak-repair'] = 'services/gas_leak_repair';
$route['services/slab-leak-repair'] = 'services/slab_leak_repair';
$route['services/water-heater-repair'] = 'services/waterheaters';
$route['services/garbage-disposal-replacement'] = 'services/disposals';
$route['services/bathtub-faucet-repair'] = 'services/bathtub_faucet_repair';
$route['services/shower-faucet-repair'] = 'services/shower_faucet_repair';
$route['services/sink-faucet-repair'] = 'services/sink_faucet_repair';
$route['services/lavatory-faucets-repair'] = 'services/lavatory_faucet_repair';
$route['services/sewer-and-drain-cleaning'] = 'services/draincleaning';
$route['services/toilet-repair'] = 'services/toilet_repair';
$route['services/bathroom-remodeling-service'] = 'services/bathroom_remodeling';
$route['services/pressure-test-plumbing'] = 'services/pressure_test_plumbing';

// BLOG
$route['articles/11-easy-plumbing-system-resolutions-for-2019'] = 'articles/plumbing_resolutions';


/* End of file routes.php */
/* Location: ./application/config/routes.php */