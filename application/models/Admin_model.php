<?php
class Admin_model extends CI_Model {
	
    public function __construct() {
        $this->load->database();
    }
    
    public function get_employees() {
	    $this->db->select('*');
        $this->db->from('employees');
        $this->db->order_by('visible', 'DESC');
        $this->db->order_by('sort', 'ASC');
        $this->db->order_by('last_name', 'DESC');

        $q = $this->db->get();
        return $q->result_array();
    }
    
        public function get_employee($eid) {
            $this->db->select('*');
            $this->db->from('employees');
            $this->db->where('eid', $eid);

            $q = $this->db->get();
            return $q->row();
        }

    public function get_applicants() {
	    $this->db->select('*');
        $this->db->from('applicants');

        $q = $this->db->get();
        return $q->result_array();
    }  

}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */