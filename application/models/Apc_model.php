<?php
class Apc_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_employees() {
        $this->db->select('*');
        $this->db->from('employees');
        $this->db->where('visible', '1');
        $this->db->order_by('sort', 'ASC');
        $this->db->order_by('last_name', 'DESC');

        $q = $this->db->get();
        return $q->result_array();
    }

        public function get_employee($eid) {
            $this->db->select('*');
            $this->db->from('employees');
            $this->db->where('eid', $eid);

            $q = $this->db->get();
            return $q->row();
        }

    public function get_testimonials() {
        $this->db->select('*');
        $this->db->from('testimonials');
        $this->db->where('visible', '1');
        $this->db->order_by('id', 'random');
        $this->db->limit(2);

        $q = $this->db->get();
        return $q->result_array();
    }

    public function get_faqs() {
        $this->db->select('*');
        $this->db->from('faqs');
        $this->db->where('active', '1');
        //$this->db->where('id >= 2');

        $q = $this->db->get();
        return $q->result_array();
    }
}

/* End of file apc_model.php */
/* Location: ./application/models/apc_model.php */